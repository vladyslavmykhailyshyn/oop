﻿namespace OOP;

public class Waiter: Employee
{
    private double Tips = 0;

    public string[] Languages;
    
    private const double WAITER_SALARY_COEF = 1.2;

    public Waiter(string name, double experience, float salary) : base(name, experience, salary)
    {
        
    }
    
    public void CollectTips(double tips)
    {
        Tips += tips;
    }

    public override void Work(string language)
    {
        var textToSay = Languages.Contains(language)
            ? $"Hi, yes I speak {language} and want to get your order"
            : $"Hi, sorry I don`t speak {language} and can`t get your order";
        
        Console.WriteLine(textToSay);
    }

    public override void GetSalary(float salary)
    {
        var textToSay = salary == (Salary * WAITER_SALARY_COEF) ? "Thank you, I`m happy waiter" : "Salary amount doesn't match, I`m sad waiter";
        Console.WriteLine(textToSay);
    }
}