﻿namespace OOP;

public class GateLogEntry
{
    public DateTime Time { get; set; }
    
    public GateAction Action { get; set; }
    
    public Person Person { get; set; }
}

public enum GateAction
{
    Entry,
    Exit
}