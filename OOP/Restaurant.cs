﻿namespace OOP;

public class Restaurant
{
    public string Name;

    public int[] Array;

    private Employee[] Team;

    public Dictionary<string, string> People = new ();

    public Restaurant(string name)
    {
        Name = name;
    }

    public void Hire(Employee employee)
    {
        
    }

    public Employee[] GetTeam()
    {
        return Team;
    }
    
    public void Hire(Employee[] employees)
    {
        if (employees == null) throw new ArgumentNullException(nameof(employees));
        
        foreach (var employee in employees)
        {
            Hire(employee);
        }
    }
}