﻿namespace OOP;

public abstract class Employee: Person, IWorker 
{
    private string Name;
    public double Experience { get; protected set; }

    private string Task;

    protected float Salary { get; private set; }

    protected Employee(string name, double experience, float salary)
    {
        Name = name;
        Experience = experience;
        Salary = salary;
    }
    
    public virtual void GetSalary(float salary)
    {
        var textToSay = salary == Salary ? "Thank you" : "Salary amount doesn't match";
        Console.WriteLine(textToSay);
    }

    public virtual void Work(string input)
    {
        throw new NotImplementedException();
    }

    public void SetTask(string task, Employee assigner)
    {
        if (assigner is Manager)
        {
            Task = task;
            Console.WriteLine("Starting working on the task");
        }
        else
        {
            Console.WriteLine("I`m not fool, you`re not a manager");
        }
    }
}