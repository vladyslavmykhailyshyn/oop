﻿namespace OOP;

public class Chef: Employee
{
    public string[] Meals;
    
    private const double CHEF_SALARY_COEF = 1.4;

    public Chef(string name, double experience, float salary) : base(name, experience, salary)
    {
        
    }

    public override void Work(string order)
    {
        var textToSay = Meals.Contains(order)
            ? $"Hi, yes I can cook a {order}"
            : $"Hi, sorry I don`t know how to cook a {order}";
        
        Console.WriteLine(textToSay);
    }
    
    public override void GetSalary(float salary)
    {
        var textToSay = salary == (Salary * CHEF_SALARY_COEF) ? "Thank you, I`m happy waiter" : "Salary amount doesn't match, I`m sad waiter";
        Console.WriteLine(textToSay);
    }
}