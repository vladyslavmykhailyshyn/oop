﻿namespace OOP;

public interface IWorker
{
    public void Work(string input);

    public void SetTask(string task, Employee assigner);
}