﻿namespace OOP;

public class GateSystem
{
    public List<GateLogEntry> ClientLog = new();
    
    public int ClientsAmount { get; private set; }
    public int CurrentClientsAmount { get; private set; }

    public List<GateLogEntry> EmployeeLog = new();

    public void PassThrough(Person person, GateAction action)
    {
        var logEntry = new GateLogEntry()
        {
            Action = action,
            Person = person,
            Time = DateTime.Now
        };
        
        if (person is Client)
        {
            ClientsAmount++;

            CurrentClientsAmount = action == GateAction.Entry ? CurrentClientsAmount + 1 : CurrentClientsAmount - 1;
            ClientLog.Add(logEntry);
        }
        else if (person is Employee)
        {
            EmployeeLog.Add(logEntry);
        }
    }
}