﻿namespace OOP;

public class GateAnalyticService
{
    private readonly GateSystem _gateSystem;
    
    public GateAnalyticService(GateSystem gateSystem)
    {
        _gateSystem = gateSystem;
    }

    public int GetClientsAmountAt(DateTime time)
    {
        var clientsAmount = 0;

        foreach (var log in _gateSystem.ClientLog)
        {
            if (log.Time > time) break;

            clientsAmount = log.Action == GateAction.Entry ? clientsAmount + 1 : clientsAmount - 1;
        }

        return clientsAmount;
    }

    public double GetEmployeeWorkingHours(Employee employee)
    {
        var referenceTime = DateTime.Now;
        var workingTime = referenceTime.Ticks;

        foreach (var log in _gateSystem.EmployeeLog)
        {
            if (log.Person.Name == employee.Name)
            {
                workingTime = log.Action == GateAction.Entry
                    ? workingTime - log.Time.Ticks
                    : workingTime + log.Time.Ticks;
            }
        }

        workingTime -= referenceTime.Ticks;
        return (workingTime / 10000000);
    }
}