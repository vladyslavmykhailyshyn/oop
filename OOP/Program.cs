﻿using OOP;

class Program
{
    public static void Main(string[] args)
    {

        Restaurant restaurant = new Restaurant("AI");

        var gateSystem = new GateSystem();

        var waiter1 = new Waiter("Waiter1", 2.4, 1200);
        var waiter2 = new Waiter("Waiter2", 4.3, 1700);
        var waiter3 = new Waiter("Waiter3", 0.8, 1400);
        var manager = new Manager("Manager", 7.4, 1700);

        Employee[] waiters = { waiter1, waiter2, waiter3 };

        // restaurant.Hire(waiters);
        // restaurant.Hire(manager);
        // IWorker worker = waiter1;
        // worker.Work(":");
        // manager.AssignTask(worker, "Do your work");
        
        // waiter2.SetTask("Do work", manager);
        // waiter2.SetTask("Do work", waiter1);
        
        // waiters[0].GetSalary(1440);
        // waiters[1].GetSalary(1700);

        var team = restaurant.GetTeam();
        
        gateSystem.PassThrough(waiter1, GateAction.Entry);
        Thread.Sleep(3000);
        Thread.Sleep(3000);
        gateSystem.PassThrough(waiter1, GateAction.Exit);
        gateSystem.PassThrough(waiter1, GateAction.Entry);
        Thread.Sleep(3000);
        gateSystem.PassThrough(waiter1, GateAction.Exit);
        gateSystem.PassThrough(waiter1, GateAction.Entry);
        Thread.Sleep(3000);
        gateSystem.PassThrough(waiter1, GateAction.Exit);


        var gateAnalytics = new GateAnalyticService(gateSystem);
        
        Console.WriteLine(gateAnalytics.GetEmployeeWorkingHours(waiter1)/3600);


        //Console.WriteLine("Hello world");
    }
}
