﻿namespace OOP;

public class Manager: Employee
{
    private string Title;

    private const double MANAGER_SALARY_COEF = 1.5;
    
    public Manager(string name, double experience, float salary) : base(name, experience, salary)
    {
    }


    public void AssignTask(IWorker employee, string task)
    {
        employee.SetTask(task, this);
        employee.Work(task);
    }
    
    public override void GetSalary(float salary)
    {
        var textToSay = salary == (Salary * MANAGER_SALARY_COEF) ? "Thank you, I`m happy manager" : "Salary amount doesn't match, I`m sad manager";
        Console.WriteLine(textToSay);
    }
}